package net.poundex.bookthing;

import generica.core.EntityClient;
import net.poundex.bookthing.dto.AuthorShortDto;
import net.poundex.bookthing.dto.BookDto;

public interface AuthorClient extends EntityClient<AuthorShortDto> {
}

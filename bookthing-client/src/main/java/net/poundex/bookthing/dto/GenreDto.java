package net.poundex.bookthing.dto;

import lombok.Data;

@Data
public class GenreDto extends GenreShortDto {
    private GenreShortDto parent;
}

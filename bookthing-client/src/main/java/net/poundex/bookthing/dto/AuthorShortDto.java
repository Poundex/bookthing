package net.poundex.bookthing.dto;

import generica.core.Dto;
import lombok.Data;

@Data
public class AuthorShortDto implements Dto {
    private Long id;
    private String name;
}

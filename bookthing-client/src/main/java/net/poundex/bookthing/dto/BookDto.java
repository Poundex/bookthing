package net.poundex.bookthing.dto;

import generica.core.Dto;
import lombok.Data;

import java.util.Set;

@Data
public class BookDto implements Dto {
    private Long id;
    private String title;
    private PublisherShortDto publisher;
    private Set<AuthorShortDto> authors;
    private Set<GenreShortDto> genres;
}

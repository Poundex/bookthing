package net.poundex.bookthing;

import generica.core.EntityClient;
import net.poundex.bookthing.dto.AuthorShortDto;
import net.poundex.bookthing.dto.PublisherShortDto;

public interface PublisherClient extends EntityClient<PublisherShortDto> {
}

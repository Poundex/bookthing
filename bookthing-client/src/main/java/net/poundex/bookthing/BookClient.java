package net.poundex.bookthing;

import generica.core.EntityClient;
import net.poundex.bookthing.dto.BookDto;

public interface BookClient extends EntityClient<BookDto> {
}

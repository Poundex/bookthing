package net.poundex.bookthing;

import generica.core.EntityClient;
import net.poundex.bookthing.dto.GenreDto;

public interface GenreClient extends EntityClient<GenreDto> {
}

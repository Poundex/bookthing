package net.poundex.bookthing.ui.web;

import generica.ui.ctx.UiContext;
import generica.ui.web.UiController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/")
public class HomeController extends UiController {

    public HomeController(UiContext uiContext) {
        super(uiContext);
    }

    @GetMapping
    public ModelAndView index() {
        return render("home").navKey("HOME").render();
    }
}

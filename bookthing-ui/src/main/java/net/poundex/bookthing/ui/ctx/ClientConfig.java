package net.poundex.bookthing.ui.ctx;

import com.fasterxml.jackson.databind.ObjectMapper;
import feign.Feign;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import lombok.RequiredArgsConstructor;
import net.poundex.bookthing.AuthorClient;
import net.poundex.bookthing.BookClient;
import net.poundex.bookthing.GenreClient;
import net.poundex.bookthing.PublisherClient;
import net.poundex.generica.ui.FeignErrorDecoder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class ClientConfig {

    private final ObjectMapper objectMapper;
    private final FeignErrorDecoder feignErrorDecoder;

    @Bean
    public BookClient bookClient() {
        return buildFeignClient(BookClient.class, "http://localhost:8080/book");
    }

    @Bean
    public PublisherClient publisherClient() {
        return buildFeignClient(PublisherClient.class, "http://localhost:8080/publisher");
    }

    @Bean
    public AuthorClient authorClient() {
        return buildFeignClient(AuthorClient.class, "http://localhost:8080/author");
    }

    @Bean
    public GenreClient genreClient() {
        return buildFeignClient(GenreClient.class, "http://localhost:8080/genre");
    }

    private <T> T buildFeignClient(Class<T> klass, String url) {
        return Feign.builder()
                .encoder(new JacksonEncoder())
                .decoder(new JacksonDecoder())
                .errorDecoder(feignErrorDecoder)
                .target(klass, url);
    }
}

package net.poundex.bookthing.ui;

import generica.ui.GenericaUiApplication;
import net.poundex.bookthing.ui.ctx.BookthingUiConfiguration;

public class BookthingUiApplication extends GenericaUiApplication {

	public static void main(String[] args) {
		launch(BookthingUiConfiguration.class, args);
	}
}


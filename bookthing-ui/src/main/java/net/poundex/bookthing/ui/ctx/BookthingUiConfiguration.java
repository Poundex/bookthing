package net.poundex.bookthing.ui.ctx;

import generica.ui.ctx.MainMenu;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "net.poundex.bookthing.ui")
public class BookthingUiConfiguration {

    @Bean
    public MainMenu bookThingMainMenu() {
        return new MainMenu(
                new MainMenu.MenuItem("Home", "/", "HOME"),
                new MainMenu.MenuCategory("Data Management",
                        new MainMenu.MenuItem("Books", "/book", "BOOKS"),
                        new MainMenu.MenuItem("Authors", "/author", "AUTHORS"),
                        new MainMenu.MenuItem("Genres", "/genre", "GENRES"),
                        new MainMenu.MenuItem("Publishers", "/publisher", "PUBLISHERS")));
    }

}

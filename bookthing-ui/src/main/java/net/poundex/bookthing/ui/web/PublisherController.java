package net.poundex.bookthing.ui.web;

import generica.ui.ctx.UiContext;
import generica.ui.web.CrudUiController;
import net.poundex.bookthing.PublisherClient;
import net.poundex.bookthing.dto.PublisherShortDto;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.net.URI;

@Controller
@RequestMapping("/publisher")
public class PublisherController extends CrudUiController<PublisherShortDto, PublisherClient> {

    public PublisherController(UiContext uiContext, PublisherClient publisherClient) {
        super(uiContext, "publisher", publisherClient, "PUBLISHERS", PublisherShortDto.class);
    }


    @Override
    protected URI createLink() {
        return URI.create("/publisher/");
    }
}

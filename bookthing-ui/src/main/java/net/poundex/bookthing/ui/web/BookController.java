package net.poundex.bookthing.ui.web;

import generica.ui.ctx.UiContext;
import generica.ui.web.CrudUiController;
import generica.ui.web.Renderable;
import net.poundex.bookthing.BookClient;
import net.poundex.bookthing.PublisherClient;
import net.poundex.bookthing.dto.BookDto;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.net.URI;

@Controller
@RequestMapping("/book")
public class BookController extends CrudUiController<BookDto, BookClient> {

    private final PublisherClient publisherClient;

    public BookController(UiContext uiContext, BookClient bookClient, PublisherClient publisherClient) {
        super(uiContext, "book", bookClient, "BOOKS", BookDto.class);
        this.publisherClient = publisherClient;
    }

    @Override
    public Renderable renderEditForm(BookDto entity) {
        return super.renderEditForm(entity)
                .withModelObject("publishers",
                        publisherClient.findAllUnpaged());
    }

    @Override
    protected URI createLink() {
        return URI.create("/book/");
    }
}

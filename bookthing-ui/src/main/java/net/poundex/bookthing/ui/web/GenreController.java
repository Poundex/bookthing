package net.poundex.bookthing.ui.web;

import generica.ui.ctx.UiContext;
import generica.ui.web.CrudUiController;
import generica.ui.web.Renderable;
import net.poundex.bookthing.GenreClient;
import net.poundex.bookthing.dto.GenreDto;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.net.URI;

@Controller
@RequestMapping("/genre")
public class GenreController extends CrudUiController<GenreDto, GenreClient> {

    public GenreController(UiContext uiContext, GenreClient genreClient) {
        super(uiContext, "genre", genreClient, "GENRES", GenreDto.class);
    }

    @Override
    public Renderable renderEditForm(GenreDto entity) {
        return super.renderEditForm(entity).withModelObject("genres", entityClient.findAllUnpaged());
    }

    @Override
    protected URI createLink() {
        return URI.create("/genre/");
    }
}

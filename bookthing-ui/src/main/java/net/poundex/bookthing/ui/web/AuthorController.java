package net.poundex.bookthing.ui.web;

import generica.ui.ctx.UiContext;
import generica.ui.web.CrudUiController;
import net.poundex.bookthing.AuthorClient;
import net.poundex.bookthing.dto.AuthorShortDto;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.net.URI;

@Controller
@RequestMapping("/author")
public class AuthorController extends CrudUiController<AuthorShortDto, AuthorClient> {

    public AuthorController(UiContext uiContext, AuthorClient authorClient) {
        super(uiContext, "author", authorClient, "AUTHORS", AuthorShortDto.class);
    }

    @Override
    protected URI createLink() {
        return URI.create("/author/");
    }
}

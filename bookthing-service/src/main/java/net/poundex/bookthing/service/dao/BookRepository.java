package net.poundex.bookthing.service.dao;

import net.poundex.bookthing.service.domain.Book;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface BookRepository extends PagingAndSortingRepository<Book, Long> {
}

package net.poundex.bookthing.service.web;

import generica.crud.web.CrudController;
import net.poundex.bookthing.dto.PublisherShortDto;
import net.poundex.bookthing.service.dao.PublisherRepository;
import net.poundex.bookthing.service.domain.Publisher;
import net.poundex.generica.crud.service.SearchService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Validator;

@Controller
@RequestMapping("/publisher")
public class PublisherController extends CrudController<Publisher, PublisherShortDto> {


    public PublisherController(PublisherRepository publisherRepository, Validator validator, SearchService searchService) {
        super(publisherRepository, PublisherShortDto.class, Publisher.class, validator, searchService);
    }

    @Override
    protected Publisher bind(PublisherShortDto dto, Publisher entity) {
        entity.setName(dto.getName());
        return entity;
    }
}

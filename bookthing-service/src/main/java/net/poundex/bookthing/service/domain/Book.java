package net.poundex.bookthing.service.domain;

import generica.crud.domain.AbstractEntity;
import generica.crud.search.DefaultContext;
import generica.crud.search.Field;
import generica.crud.search.Searchable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Set;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toSet;

@Entity
@Getter @Setter
@AllArgsConstructor @NoArgsConstructor
@Indexed @Searchable(fields = { @Field("title"), @Field("authors") })
public class Book extends AbstractEntity {

    @NotBlank
    @FullTextField(analyzer = DefaultContext.STANDARD_ANALYZER)
    private String title;

    @ManyToOne
    @NotNull
    @IndexedEmbedded
    private Publisher publisher;

    @ManyToMany
    @JoinTable(name = "book_author",
            joinColumns = @JoinColumn(name =  "book_id"),
            inverseJoinColumns = @JoinColumn(name = "author_id"))
    @NotEmpty
    @IndexedEmbedded(name = "author")
    private Set<Author> authors;

    @ManyToMany
    @JoinTable(name = "book_genre",
            joinColumns = @JoinColumn(name =  "book_id"),
            inverseJoinColumns = @JoinColumn(name = "genre_id"))
    @NotEmpty
    private Set<Genre> genres;

    @IndexedEmbedded(name = "genre")
    @AssociationInverseSide(inversePath = @ObjectPath(@PropertyValue(propertyName = "books")))
    @IndexingDependency(derivedFrom = @ObjectPath(@PropertyValue(propertyName = "genres")))
    public Set<Genre> getAllGenres() {
        return Stream.concat(genres.stream(), genres.stream()
                .flatMap(g -> g.getAllParents().stream()))
                .collect(toSet());
    }
}

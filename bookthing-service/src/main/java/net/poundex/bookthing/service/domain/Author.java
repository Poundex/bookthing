package net.poundex.bookthing.service.domain;

import generica.crud.domain.AbstractEntity;
import generica.crud.search.DefaultContext;
import generica.crud.search.Field;
import generica.crud.search.Searchable;
import lombok.*;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.FullTextField;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.Indexed;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Set;

@Entity
@Getter @Setter
@AllArgsConstructor @NoArgsConstructor
@Indexed @Searchable(fields = { @Field("name") })
public class Author extends AbstractEntity {
    @NotBlank
    @FullTextField(analyzer = DefaultContext.STANDARD_ANALYZER)
    private String name;

    @ManyToMany(mappedBy = "authors")
    private Set<Book> books;
}

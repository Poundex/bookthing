package net.poundex.bookthing.service.ctx;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@ComponentScan(basePackages = "net.poundex.bookthing.service")
@EnableJpaRepositories(basePackages = "net.poundex.bookthing.service.dao")
@EntityScan(basePackages = "net.poundex.bookthing.service.domain")
public class BookthingServiceConfiguration {

}

package net.poundex.bookthing.service.web;

import generica.crud.web.CrudController;
import net.poundex.bookthing.dto.AuthorShortDto;
import net.poundex.bookthing.service.dao.AuthorRepository;
import net.poundex.bookthing.service.domain.Author;
import net.poundex.generica.crud.service.SearchService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Validator;

@Controller
@RequestMapping("/author")
public class AuthorController extends CrudController<Author, AuthorShortDto> {

    public AuthorController(AuthorRepository authorRepository, Validator validator, SearchService searchService) {
        super(authorRepository, AuthorShortDto.class, Author.class, validator, searchService);
    }

    @Override
    protected Author bind(AuthorShortDto dto, Author entity) {
        entity.setName(dto.getName());
        return entity;
    }
}

package net.poundex.bookthing.service.dao;

import net.poundex.bookthing.service.domain.Publisher;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PublisherRepository extends PagingAndSortingRepository<Publisher, Long> {
}

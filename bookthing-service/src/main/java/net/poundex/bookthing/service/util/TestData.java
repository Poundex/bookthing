package net.poundex.bookthing.service.util;

import lombok.RequiredArgsConstructor;
import net.poundex.bookthing.service.dao.AuthorRepository;
import net.poundex.bookthing.service.dao.BookRepository;
import net.poundex.bookthing.service.dao.GenreRepository;
import net.poundex.bookthing.service.dao.PublisherRepository;
import net.poundex.bookthing.service.domain.Author;
import net.poundex.bookthing.service.domain.Book;
import net.poundex.bookthing.service.domain.Genre;
import net.poundex.bookthing.service.domain.Publisher;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.stream.IntStream;

@Component
@RequiredArgsConstructor
public class TestData implements CommandLineRunner {

    private final BookRepository bookRepository;
    private final AuthorRepository authorRepository;
    private final PublisherRepository publisherRepository;
    private final GenreRepository genreRepository;

    @Override
    public void run(String... args) throws Exception {
        Publisher ficPub = publisherRepository.save(
                new Publisher("Fiction Publishing", null));

        Publisher tekPub = publisherRepository.save(
                new Publisher("Technopress", null));

        Author aOne = authorRepository.save(
                new Author("Humbert Wheelton", null));

        Author aTwo = authorRepository.save(
                new Author("Shalne Peete", null));

        Author aThree = authorRepository.save(
                new Author("Mirilla Folk", null));

        Author aFour = authorRepository.save(
                new Author("Shandee Edon", null));

        Author aFive = authorRepository.save(
                new Author("Hattie Delooze", null));

        Author aSix = authorRepository.save(
                new Author("Thorvald O'Hagan", null));

        Genre fiction = genreRepository.save(new Genre( "Fiction", null, null, null));
        Genre fantasy = genreRepository.save(new Genre( "Fantasy", fiction, null, null));
        Genre mystery = genreRepository.save(new Genre( "Mystery", fiction, null, null));

        Genre nonfiction = genreRepository.save(new Genre("Non-fiction", null, null, null));
        Genre science = genreRepository.save(new Genre("Science", nonfiction, null, null));

        Book b1 = bookRepository.save(new Book("The Prey in the City", ficPub, Set.of(aOne), Set.of(fantasy)));
        Book b2 = bookRepository.save(new Book("Clue of the Claw", ficPub, Set.of(aTwo), Set.of(mystery)));
        Book b3 = bookRepository.save(new Book("The Missing King", ficPub, Set.of(aThree), Set.of(fantasy, mystery)));

        Book b4 = bookRepository.save(new Book("Flux Capacitors for Dummies", tekPub, Set.of(aFour, aFive), Set.of(science)));
        Book b5 = bookRepository.save(new Book("Quantum Plasma in Practice", tekPub, Set.of(aSix), Set.of(science)));

        IntStream.rangeClosed(0, 50).forEach(i ->
                publisherRepository.save(new Publisher("Test Publisher " + i, null)));
    }
}

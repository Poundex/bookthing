package net.poundex.bookthing.service.dao;

import net.poundex.bookthing.service.domain.Author;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface AuthorRepository extends PagingAndSortingRepository<Author, Long> {
}

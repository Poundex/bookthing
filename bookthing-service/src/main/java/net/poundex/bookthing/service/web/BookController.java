package net.poundex.bookthing.service.web;

import generica.crud.web.CrudController;
import net.poundex.bookthing.dto.BookDto;
import net.poundex.bookthing.service.dao.AuthorRepository;
import net.poundex.bookthing.service.dao.BookRepository;
import net.poundex.bookthing.service.dao.GenreRepository;
import net.poundex.bookthing.service.dao.PublisherRepository;
import net.poundex.bookthing.service.domain.Author;
import net.poundex.bookthing.service.domain.Book;
import net.poundex.bookthing.service.domain.Genre;
import net.poundex.bookthing.service.domain.Publisher;
import net.poundex.generica.crud.service.SearchService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Validator;

@Controller
@RequestMapping("/book")
public class BookController extends CrudController<Book, BookDto> {

    private final PublisherRepository publisherRepository;
    private final AuthorRepository authorRepository;
    private final GenreRepository genreRepository;

    public BookController(BookRepository bookRepository, Validator validator, SearchService searchService, PublisherRepository publisherRepository, AuthorRepository authorRepository, GenreRepository genreRepository) {
        super(bookRepository, BookDto.class, Book.class, validator, searchService);
        this.publisherRepository = publisherRepository;
        this.authorRepository = authorRepository;
        this.genreRepository = genreRepository;
    }

    @Override
    protected Book bind(BookDto dto, Book entity) {
        entity.setTitle(dto.getTitle());
        entity.setPublisher(resolveSingle(dto.getPublisher(), publisherRepository, entity, Publisher.class));
        entity.setAuthors(resolveMany(dto.getAuthors(), authorRepository, entity, Author.class));
        entity.setGenres(resolveMany(dto.getGenres(), genreRepository, entity, Genre.class));
        return entity;
    }
}

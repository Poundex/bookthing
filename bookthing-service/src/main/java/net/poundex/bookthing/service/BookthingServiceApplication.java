package net.poundex.bookthing.service;

import generica.crud.GenericaCrudServiceApplication;
import net.poundex.bookthing.service.ctx.BookthingServiceConfiguration;

public class BookthingServiceApplication extends GenericaCrudServiceApplication {

	public static void main(String[] args) {
		launch(BookthingServiceConfiguration.class, args);
	}

}

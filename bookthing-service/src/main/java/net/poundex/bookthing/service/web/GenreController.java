package net.poundex.bookthing.service.web;

import generica.crud.web.CrudController;
import net.poundex.bookthing.dto.GenreDto;
import net.poundex.bookthing.service.dao.GenreRepository;
import net.poundex.bookthing.service.domain.Genre;
import net.poundex.generica.crud.service.SearchService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Validator;

@Controller
@RequestMapping("/genre")
public class GenreController extends CrudController<Genre, GenreDto> {

    public GenreController(GenreRepository genreRepository, Validator validator, SearchService searchService) {
        super(genreRepository, GenreDto.class, Genre.class, validator, searchService);
    }

    @Override
    protected Genre bind(GenreDto dto, Genre entity) {
        entity.setName(dto.getName());
        entity.setParent(resolveSingle(dto.getParent(), repository, entity, Genre.class));
        return entity;
    }
}

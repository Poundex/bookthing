package net.poundex.bookthing.service.domain;

import generica.crud.domain.AbstractEntity;
import generica.crud.search.DefaultContext;
import generica.crud.search.Field;
import generica.crud.search.Searchable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.*;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Entity
@Getter @Setter
@AllArgsConstructor @NoArgsConstructor
@Indexed @Searchable(fields = { @Field("name") })
public class Genre extends AbstractEntity {
    @NotBlank
    @FullTextField(analyzer = DefaultContext.STANDARD_ANALYZER)
    private String name;

    @ManyToOne
    private Genre parent;

    @IndexedEmbedded(name = "parent", includePaths = {"id", "name"})
    @IndexingDependency(derivedFrom = @ObjectPath(@PropertyValue(propertyName = "parent")))
    @AssociationInverseSide(inversePath = @ObjectPath(@PropertyValue(propertyName = "children")))
    public List<Genre> getAllParents() {
        if (parent == null)
            return Collections.emptyList();

        return Stream.concat(
                Stream.of(parent),
                parent.getAllParents().stream())
                .collect(Collectors.toList());
    }

    @ManyToMany(mappedBy = "genres")
    private Set<Book> books;

    @OneToMany(mappedBy = "parent")
    private Set<Genre> children;
}

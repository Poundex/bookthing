package net.poundex.bookthing.service.dao;

import net.poundex.bookthing.service.domain.Genre;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface GenreRepository extends PagingAndSortingRepository<Genre, Long> {
}
